package com.newpartner.express.utils;

import ir.mahdi.mzip.zip.ZipArchive;

@SuppressWarnings("ConstantConditions")
public class FileUtil {

    @SuppressWarnings("unused")
    public static void unzip() {
        ZipArchive.unzip("/sdcard/file.zip", "/sdcard/folder", "");
    }
}
