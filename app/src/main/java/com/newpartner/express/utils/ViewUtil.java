package com.newpartner.express.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.newpartner.express.R;

import java.lang.reflect.Method;

import timber.log.Timber;

@SuppressWarnings("ConstantConditions")
public class ViewUtil {

    @SuppressWarnings("unused")
    public static int dp2px(float dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    @SuppressWarnings("unused")
    public static Point getWindow(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    @SuppressWarnings("unused")
    public static Point getScreen(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            display.getRealSize(size);
        } else {
            Method methodGetRawHeight, methodGetRawWidth;
            try {
                methodGetRawHeight = Display.class.getMethod("getRawHeight");
                methodGetRawWidth = Display.class.getMethod("getRawWidth");
                size.y = (Integer) methodGetRawHeight.invoke(display);
                size.x = (Integer) methodGetRawWidth.invoke(display);
            } catch (Exception e) {
                Timber.e(e.getMessage());
            }
        }
        return size;
    }

    @SuppressWarnings("unused")
    public static void showKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE))
                .toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    @SuppressWarnings("unused")
    public static void hideKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE))
                .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }

    @SuppressWarnings("unused")
    public static void showProgressDialog(Activity activity) {
        new MaterialDialog.Builder(activity)
            .title(R.string.progress)
            .progress(true, 0)
            //.cancelable(false)
            .show();
    }
}
