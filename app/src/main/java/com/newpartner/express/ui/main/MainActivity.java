package com.newpartner.express.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.google.firebase.auth.FirebaseAuth;
import com.newpartner.express.R;
import com.newpartner.express.ui.base.BaseActivity;
import com.newpartner.express.ui.base.BaseFragment;
import com.newpartner.express.ui.fragments.AddressBookFragment;
import com.newpartner.express.ui.fragments.BalanceFragment;
import com.newpartner.express.ui.fragments.MyAddressesFragment;
import com.newpartner.express.ui.fragments.MyOrdersFragment;
import com.newpartner.express.ui.fragments.NewOrderFragment;
import com.newpartner.express.ui.login.LoginActivity;

import timber.log.Timber;

public class MainActivity extends BaseActivity
	implements NavigationView.OnNavigationItemSelectedListener {

	@BindView(R.id.drawer_layout)
	DrawerLayout drawer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle(R.string.action_my_orders);
		ButterKnife.bind(this);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, 0, 0);
		drawer.addDrawerListener(toggle);
		toggle.syncState();
		NavigationView navigationView = findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
		navigationView.getMenu().getItem(0).setChecked(true);

		setFragment(MyOrdersFragment.class);
	}

	@Override
	public void onBackPressed() {
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			finish();
		}
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_my_orders:
				setTitle(R.string.action_my_orders);
				setFragment(MyOrdersFragment.class);
				break;
			case R.id.action_new_order:
				setTitle(R.string.action_new_order);
				setFragment(NewOrderFragment.class);
				break;
			case R.id.action_my_addresses:
				setTitle(R.string.action_my_addresses);
				setFragment(MyAddressesFragment.class);
				break;
			case R.id.action_address_book:
				setTitle(R.string.action_address_book);
				setFragment(AddressBookFragment.class);
				break;
			case R.id.action_balance:
				setTitle(R.string.action_balance);
				setFragment(BalanceFragment.class);
				break;
			case R.id.action_exit_app:
				try {
					FirebaseAuth.getInstance()
						.signOut();
				} finally {
					switchActivity(LoginActivity.class);
				}
				break;
		}
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	private void setFragment(Class<? extends BaseFragment> fragmentClass) {
		try {
			BaseFragment fragment = fragmentClass.getConstructor().newInstance();
			getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.container, fragment)
				.commit();
		} catch (Exception e) {
			Timber.e(e);
		}
	}
}