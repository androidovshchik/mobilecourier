package com.newpartner.express.ui.steps;

import android.content.Context;
import android.support.annotation.IdRes;
import android.util.AttributeSet;
import android.widget.RadioGroup;

import com.newpartner.express.R;

public class WhoAmIStepView extends RadioGroup {

	public boolean isSender;

	public WhoAmIStepView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnCheckedChangeListener((RadioGroup group, @IdRes int checkedId) ->
			isSender = checkedId == R.id.sender);
	}

	@Override
	public boolean hasOverlappingRendering() {
		return false;
	}
}