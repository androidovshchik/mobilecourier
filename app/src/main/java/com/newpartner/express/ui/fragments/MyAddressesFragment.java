package com.newpartner.express.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

import com.newpartner.express.R;
import com.newpartner.express.ui.base.BaseFragment;

public class MyAddressesFragment extends BaseFragment {

	public MyAddressesFragment() {}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_my_addresses, container, false);
		unbinder = ButterKnife.bind(this, view);
		return view;
	}
}
