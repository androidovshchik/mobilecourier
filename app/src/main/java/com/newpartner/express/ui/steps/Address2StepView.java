package com.newpartner.express.ui.steps;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.newpartner.express.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Address2StepView extends LinearLayout {

	private Unbinder unbinder;

	public String address;

	@BindView(R.id.address)
	public EditText editText;

	public Address2StepView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		unbinder = ButterKnife.bind(this);
		//editText2.addTextChangedListener();
	}

	@Override
	public void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		unbinder.unbind();
	}

	@Override
	public boolean hasOverlappingRendering() {
		return false;
	}
}