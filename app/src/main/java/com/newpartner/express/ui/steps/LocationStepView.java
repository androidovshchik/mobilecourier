package com.newpartner.express.ui.steps;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.newpartner.express.R;
import com.tbruyelle.rxpermissions2.RxPermissions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.newpartner.express.utils.ViewUtil;

public class LocationStepView extends LinearLayout {

	private Unbinder unbinder;

	private RxPermissions rxPermissions;

	public String address;

	@BindView(R.id.address)
	public EditText editText;

	public LocationStepView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Activity activity = getActivity();
		if (activity != null) {
			rxPermissions = new RxPermissions(activity);
		}
	}

	@OnClick(R.id.detect)
	public void onDetect() {
		if (rxPermissions == null) {
			return;
		}
		rxPermissions.request(Manifest.permission.ACCESS_COARSE_LOCATION)
			.subscribe(granted -> {
				if (granted) {
					ViewUtil.showProgressDialog(getActivity());
				}
			});
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		unbinder = ButterKnife.bind(this);
		//editText2.addTextChangedListener();
	}

	@Override
	public void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		unbinder.unbind();
	}

	@Override
	public boolean hasOverlappingRendering() {
		return false;
	}

	@Nullable
	private Activity getActivity() {
		Context context = getContext();
		while (context instanceof ContextWrapper) {
			if (context instanceof Activity) {
				return (Activity) context;
			}
			context = ((ContextWrapper) context).getBaseContext();
		}
		return null;
	}
}