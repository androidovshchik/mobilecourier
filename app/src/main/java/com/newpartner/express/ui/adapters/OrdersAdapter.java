package com.newpartner.express.ui.adapters;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.newpartner.express.R;
import com.newpartner.express.models.Order;
import com.newpartner.express.ui.base.BaseAdapter;
import com.newpartner.express.ui.base.BaseViewHolder;

public class OrdersAdapter extends BaseAdapter<Order, OrdersAdapter.TasksViewHolder> {

    public OrdersAdapter() {
        super();
    }

    @Override
    public TasksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task,
            parent, false);
        return new TasksViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TasksViewHolder holder, int position) {

    }

    public class TasksViewHolder extends BaseViewHolder {

        public TasksViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
