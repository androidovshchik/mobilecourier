package com.newpartner.express.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.newpartner.express.ui.login.LoginActivity;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

@SuppressWarnings("Registered")
public class BaseActivity extends AppCompatActivity {

    protected CompositeDisposable disposable = new CompositeDisposable();

    protected FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null && !(this instanceof LoginActivity)) {
            Timber.d("user: null");
            switchActivity(LoginActivity.class);
        }
    }

    protected void showMessage(@StringRes int id) {
        showMessage(getString(id));
    }

    protected void showMessage(@Nullable String message) {
        if (message == null) {
            return;
        }
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
                .show();
    }

    protected void launchActivity(Class activity) {
        Intent intent = new Intent(getApplicationContext(), activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected void switchActivity(Class activity) {
        launchActivity(activity);
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
