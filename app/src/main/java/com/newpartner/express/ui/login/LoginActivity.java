package com.newpartner.express.ui.login;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.newpartner.express.R;
import com.newpartner.express.models.User;
import com.newpartner.express.ui.main.MainActivity;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

import com.newpartner.express.ui.base.BaseActivity;

import java.util.concurrent.TimeUnit;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.phone)
    EditText phoneView;
    @BindView(R.id.get_verification_code)
    View getCodeButton;
    @BindView(R.id.verification_code)
    EditText codeView;
    @BindView(R.id.verify)
    View verifyButton;
    @BindView(R.id.attempt)
    TextView attempt;

    private String verificationId;

    private boolean attemptingCode = false;

    private VerificationCallback callback = new VerificationCallback() {

        @Override
        public void onSuccess(@Nullable PhoneAuthCredential credential,
                              @Nullable String verificationId) {
            if (credential != null) {
                signInWithPhoneAuthCredential(credential);
                return;
            }
            LoginActivity.this.verificationId = verificationId;
            toggleCode(true);
            downTimer.start();
            attemptingCode = true;
        }

        @Override
        public void onError(@StringRes int id) {
            showMessage(id);
            if (attemptingCode) {
                downTimer.cancel();
            } else {
                togglePhone(true);
            }
        }
    };

    private CountDownTimer downTimer = new CountDownTimer(60000, 1000) {

        @Override
        public void onTick(long millisUntilFinished) {
            attempt.setText(getString(R.string.next_attempt, millisUntilFinished / 1000));
        }

        @Override
        public void onFinish() {
            attemptingCode = false;
            togglePhone(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle(R.string.title_login);
        ButterKnife.bind(this);
        MaskedTextChangedListener listener = new MaskedTextChangedListener(
            "+7([000])[000]-[00]-[00]", true, phoneView, null, null);
        phoneView.addTextChangedListener(listener);
        phoneView.setOnFocusChangeListener(listener);
    }

    @OnClick(R.id.get_verification_code)
    public void onGetCode() {
        String phone = phoneView.getText().toString().trim();
        Timber.d("phone: %s", phone);
        if (phone.length() != 16) {
            showMessage(R.string.error_empty_phone);
            return;
        }
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phone, 60, TimeUnit.SECONDS,
            this, callback);
        togglePhone(false);
        attempt.setText(getString(R.string.wait));
    }

    @OnClick(R.id.verify)
    public void onVerify() {
        String code = codeView.getText().toString().trim();
        Timber.d("code: %s", code);
        if (code.length() <= 0) {
            showMessage(R.string.error_empty_verification_code);
            return;
        }
        toggleCode(false);
        signInWithPhoneAuthCredential(PhoneAuthProvider.getCredential(verificationId, code));
    }

    @SuppressWarnings("ConstantConditions")
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
            .addOnCompleteListener(this, (@NonNull Task<AuthResult> task) -> {
                if (task.isSuccessful()) {
                    Timber.d("signInWithCredential: success");
                    String uuid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    User user = new User(uuid);
                    FirebaseFirestore.getInstance()
                        .collection("users")
                        .document(uuid)
                        .set(user)
                        .addOnSuccessListener((Void data) -> switchActivity(MainActivity.class))
                        .addOnFailureListener((@NonNull Exception e) -> {
                            Timber.e(e);
                            showMessage(R.string.error_login);
                            if (attemptingCode) {
                                toggleCode(true);
                            }
                        });
                } else {
                    Timber.e(task.getException());
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        showMessage(R.string.error_phone_invalid_credentials);
                    }
                    if (attemptingCode) {
                        toggleCode(true);
                    }
                }
            });
    }

    private void togglePhone(boolean enable) {
        phoneView.setEnabled(enable);
        getCodeButton.setEnabled(enable);
        attempt.setVisibility(enable? View.INVISIBLE : View.VISIBLE);
        toggleCode(false);
    }

    private void toggleCode(boolean enable) {
        codeView.setEnabled(enable);
        verifyButton.setEnabled(enable);
        if (!enable) {
            codeView.setText("");
            codeView.clearFocus();
        }
    }
}