package com.newpartner.express.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import ernestoyaquello.com.verticalstepperform.VerticalStepperFormLayout;
import ernestoyaquello.com.verticalstepperform.interfaces.VerticalStepperForm;

import com.newpartner.express.R;
import com.newpartner.express.ui.base.BaseFragment;
import com.newpartner.express.ui.steps.LocationStepView;
import com.newpartner.express.ui.steps.WhoAmIStepView;
import timber.log.Timber;

public class NewOrderFragment extends BaseFragment implements VerticalStepperForm {

	@BindView(R.id.stepper)
	VerticalStepperFormLayout verticalStepperForm;

	private WhoAmIStepView whoAmIStepView;
	private LocationStepView locationStepView;

	public NewOrderFragment() {}

	@Override
	@SuppressWarnings("ConstantConditions")
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_new_order, container, false);
		unbinder = ButterKnife.bind(this, view);
		VerticalStepperFormLayout.Builder.newInstance(verticalStepperForm, getResources()
			.getStringArray(R.array.new_order_list), this, getActivity())
			.primaryColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
			.primaryDarkColor(ContextCompat.getColor(getApplicationContext(),
				R.color.colorPrimaryDark))
			.displayBottomNavigation(false)
			.init();
		return view;
	}

	@Override
	@SuppressWarnings("all")
	public View createStepContentView(int stepNumber) {
		switch (stepNumber) {
			case 0:
				if (whoAmIStepView == null) {
					whoAmIStepView = (WhoAmIStepView)
						getLayoutInflater().inflate(R.layout.step1, null, false);
				}
				return whoAmIStepView;
			case 1:
				if (locationStepView == null) {
					locationStepView = (LocationStepView)
						getLayoutInflater().inflate(R.layout.step2, null, false);
				}
				return locationStepView;
			default:
				return new View(getApplicationContext());
		}
	}

	@Override
	public void onStepOpening(int stepNumber) {
		switch (stepNumber) {
			case 1:
				verticalStepperForm.setActiveStepAsCompleted();
				Timber.d("Step1. isSender = %b", whoAmIStepView.isSender);
				break;
			case 2:
				//Timber.d("Step1. autoLocation = %b", autoLocation);
				break;
			default:
				verticalStepperForm.setActiveStepAsCompleted();
		}
	}

	@Override
	public void sendData() {}

	@Override
	public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
	}
}
