package com.newpartner.express.models.data;

import android.content.ContentValues;
import android.database.Cursor;

public abstract class Row {

	public abstract ContentValues toContentValues();

	public abstract void parseCursor(Cursor cursor);

	public abstract String getTable();
}
