package com.newpartner.express.models.data;

import android.content.ContentValues;
import android.database.Cursor;

public class Element extends Row {

	public static final String TABLE = "elements";

	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_SECTION2 = "section2";
	public static final String COLUMN_CODE = "code";
	public static final String COLUMN_CATALOG = "catalog";

	public long id;

	public String name;

	public long section2;

	public String code;

	public long catalog;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(COLUMN_NAME, name);
		values.put(COLUMN_SECTION2, section2);
		values.put(COLUMN_CODE, code);
		values.put(COLUMN_CATALOG, catalog);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
		section2 = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_SECTION2));
		code = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_CODE));
		catalog = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_CATALOG));
	}

	@Override
	public String getTable() {
		return TABLE;
	}
}
