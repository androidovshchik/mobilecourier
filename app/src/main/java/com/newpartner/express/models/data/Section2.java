package com.newpartner.express.models.data;

import android.content.ContentValues;
import android.database.Cursor;

public class Section2 extends Row {

	public static final String TABLE = "sections2";

	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_SECTION1 = "section1";

	public long id;

	public String name;

	public long section1;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(COLUMN_NAME, name);
		values.put(COLUMN_SECTION1, section1);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
		section1 = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_SECTION1));
	}

	@Override
	public String getTable() {
		return TABLE;
	}
}
