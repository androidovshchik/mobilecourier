package com.newpartner.express.models;

import com.google.firebase.firestore.Exclude;

public class User {

	private static final long DEFAULT_1C_ID = 123456789;

	@Exclude
	public String fireId;

	public long oneCId;

	public User(String fireId) {
		this.fireId = fireId;
		this.oneCId = DEFAULT_1C_ID;
	}
}
