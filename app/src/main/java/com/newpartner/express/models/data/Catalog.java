package com.newpartner.express.models.data;

import android.content.ContentValues;
import android.database.Cursor;

public class Catalog extends Row {

	public static final String TABLE = "catalog";

	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NAME = "name";

	public long id;

	public String name;

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(COLUMN_NAME, name);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME));
	}

	@Override
	public String getTable() {
		return TABLE;
	}
}
